import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from pyplot_lib.zoom_pan import ZoomPan


def plot_xy_i(x, clr, xmax=None, **kwargs):
    plt.plot(x[0], x[1].real, color=clr)
    plt.plot(x[0], x[1].imag, '--', color=clr)
    xmax = max(x[0])
    xmin = min(x[0])

    plt.grid(True)

    plt.xlim(xmin, xmax-1)
    plt.show()


def plot_reim_i(x, clr, xmax=None, **kwargs):

    plt.plot(x.real, color=clr)
    plt.plot(x.imag, '--', color=clr)

    if xmax is None:
        xmax = len(x)
    xmin = 0

    plt.grid(True)
    plt.xlim(xmin, xmax-1)
    plt.show()


def plot_psd_i(x, clr, fs, **kwargs):
    plt.psd(x, color=clr, Fs=fs/1e6, NFFT=2048, linewidth=0.8)
    plt.xlabel('Frequency, MHz')
    plt.grid(True)
    # plt.show()


def is_not_list_or_ndarray(x):
    return (type(x) is not np.ndarray) and (type(x) is not list)


def plot_handler(x, plot_func, fig=None, new_fig=True, keep=False, subplot=None, fs=300e6, **kwargs):

    plt.rc('font', size=8)

    if (type(x) is list) and (is_not_list_or_ndarray(x[0])):
        x = np.array(x)

    if is_not_list_or_ndarray(x[0]):
        x = [x]

    if type(subplot) == list:
        new_fig = False
        keep = True
        plt.subplot(subplot[0], subplot[1], subplot[2])

    # if new_fig:
    #     plt.figure(fig)

    if not keep:
        plt.clf()

    xmax = 0
    for i in range(len(x)):
        xi = x[i]
        clr = list(cm.tab10(i))
        if len(xi) > xmax:
            xmax = len(xi)
        args = {'x': xi, 'clr': clr, 'fs': fs, 'xmax':xmax}
        args.update(kwargs)
        plot_func(**args)

    ax = plt.gca()

    zp = ZoomPan()
    figZoom = zp.zoom_factory(ax, base_scale=1.1)
    figPan = zp.pan_factory(ax)

    # plt.show()
    # plt.pause(0.001)

