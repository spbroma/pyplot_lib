import numpy as np
from pyplot_lib.plot_lib_support import plot_handler, plot_reim_i, plot_psd_i, plot_xy_i


def plot_reim(x, fig=None, new_fig=True, keep=False, **kwargs):
    plot_handler(x, plot_reim_i, fig=fig, new_fig=new_fig, keep=keep, **kwargs)


def plot_psd(x, fig=None, new_fig=True, keep=False, **kwargs):
    plot_handler(x, plot_psd_i, fig=fig, new_fig=new_fig, keep=keep, **kwargs)


def plot_xy(x, fig=None, new_fig=True, keep=False, **kwargs):
    plot_handler(x, plot_xy_i, fig=fig, new_fig=new_fig, keep=keep, **kwargs)


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    from python_libs.std_lib.signal_gen import signal_gen

    N = 1000
    x = signal_gen(N)

    #
    # x = np.random.randn(N) + np.random.randn(N)*1j
    # max_x = max(np.abs(x))
    # x = x/max_x
    # x = np.array(x)

    x_list = []

    for i in range(10):
        x = x * 1.2
        x_list.append(x)

    x_list = tuple(x_list)

    plt.figure()
    plot_reim(x_list, subplot=[2, 1, 1])
    plot_psd(x_list,  subplot=[2, 1, 2])

    # plot_reim(x_list, marker='.')



