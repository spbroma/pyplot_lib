import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
from pyplot_lib.plot_lib import plot_reim, plot_psd
from scipy.signal import resample


def fir_bandpass_filter(data, lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b = signal.firwin(order, cutoff=[low, high], window="hamming", pass_zero=False)
    y = signal.lfilter(b, 1, data)
    return y


def fir_lowpass_filter(data, highcut, fs, order=5):
    nyq = 0.5 * fs
    high = highcut / nyq
    b = signal.firwin(order, cutoff=high, window="hamming")
    y = signal.lfilter(b, 1, data)
    return y


def freq_shift_to_real(x):
    N = len(x)
    fs = 1
    y = resample(x, 2 * N)
    y = y * np.exp(1j * fs / 2 * np.pi * np.arange(2 * N) / fs)
    y = y.real
    return y


def signal_gen(n=10000, bw=20e6, fs=300e6, filt_order=32, real=False):
    x_r = np.random.rand(int(n)) - 0.5
    x_i = np.random.rand(int(n)) - 0.5
    x =x_r + x_i*1j
    max_x = max(abs(x))
    x = x/max_x

    bw_arr = np.array([bw])

    if sum(bw_arr.shape) <= 2:
        if type(bw) == list:
            bw = bw[0]
        y = fir_lowpass_filter(x, bw, fs, order=filt_order)
    else:
        y = fir_bandpass_filter(x, bw[0], bw[1], fs, order=filt_order)

    y = y/max(abs(y))

    if real:
        y = freq_shift_to_real(y)
    return y


if __name__ == '__main__':
    Fs = 300e6
    BW = 30e6
    N = 20000
    filt_order = 64
    # y0 = signal_gen(N, bw=[0.8*BW, BW], fs=Fs, filt_order=filt_order, real=True)
    x = signal_gen(N, bw=BW, fs=Fs, filt_order=filt_order)
    y = freq_shift_to_real(x)

    print('plot')
    plot_psd((x, y), fs=Fs)


