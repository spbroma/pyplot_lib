# pyplot_lib

Wrapper for matplotlib.pyplot for complex signal with snap and zoom and minimalistic interface.

# Functions

- Zoom with wheel
- Pan with mouse
- Use **Ctrl** to lock x-axis and **Shift** for y-axis

# Usage

``` python
from pyplot_lib.plot_lib import plot_reim, plot_psd

import numpy as np

x = np.random.rand(100)
y = np.random.rand(100)
z = np.random.rand(100)

plot_reim(x)
plot_reim([x, y, z], fig=1)

```